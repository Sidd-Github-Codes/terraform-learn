terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region     = "ap-south-1"
}

variable "cidr_block" {
  description = "cidr block for vpc and subnet"
  type = list(string)
}


resource "aws_vpc" "development-vpc"{
    cidr_block = var.cidr_block[0]
    tags ={
        Name = "development"
    }
}

resource "aws_subnet" "dev-subnet-1"{
    vpc_id = aws_vpc.development-vpc.id
    cidr_block = var.cidr_block[1]
    availability_zone = "ap-south-1a"
    tags ={
        Name = "subnet-1-dev"
    }
}

output "dev-vpc-id" {
  value       = aws_vpc.development-vpc.id
}

output "dev-subnet-id"{
  value = aws_subnet.dev-subnet-1.id
}



